# Generated by Django 3.0 on 2019-12-12 00:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='empresas',
            new_name='Empresa',
        ),
        migrations.RenameModel(
            old_name='facturas',
            new_name='Factura',
        ),
        migrations.RenameModel(
            old_name='productos',
            new_name='Producto',
        ),
        migrations.RenameModel(
            old_name='usuarios',
            new_name='Usuario',
        ),
    ]
