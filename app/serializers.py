from rest_framework import serializers
from .models import tipos_iva, Empresa, Producto, estados_factura, Factura
'''
class ContribuyenteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contribuyente
        fields = ('nit', 'nombre_razon_social', 'telefono')

class EstadoFacturaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EstadoFactura
        fields = ('estado',)

class FacturaSerializer(serializers.HyperlinkedModelSerializer):
    
    nit_emisor = ContribuyenteSerializer()
    estado = EstadoFacturaSerializer()
    class Meta:
        model = Factura
        fields = ('numero_completo','numero_factura','prefijo','nit_emisor', 'nit_receptor', 'estado', 'fecha_factura', 'valor_factura')
'''


class TiposIvaSerializer(serializers.ModelSerializer):
    class Meta:
        model = tipos_iva
        fields = ('procentaje',)


class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ('nit_empresa', 'dv_empresa', 'estado',)


class EstadoFacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = estados_factura
        fields = ('estado',)


class FacturaSerializer(serializers.HyperlinkedModelSerializer):

    id_empresa = EmpresaSerializer()
    estado_factura = EstadoFacturaSerializer()

    class Meta:
        model = Factura
        fields = ['prefijo_factura', 'numero_factura', 'fecha_factura',
                  'valor_factura', 'valor_iva', 'id_empresa', 'nit_receptor', 'estado_factura']


class ProductoSerializer(serializers.HyperlinkedModelSerializer):

    #id_tipo_iva = TiposIvaSerializer()
    #nit_empresa = EmpresaSerializer()
    #id_factura = FacturaSerializer()

    class Meta:
        model = Producto
        fields = ('referencia', 'descripcion', 'valor_unitario',
                  'cantidad',)
        #fields = "__all__"