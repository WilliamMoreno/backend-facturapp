from django.db import models

# Create your models here.
# Empresas que estan registradas como clientes de la plataforma de facturación electrónica


class Empresa(models.Model):

    nit_empresa = models.IntegerField()
    dv_empresa = models.IntegerField()
    estado = models.NullBooleanField()

    class Meta:
        db_table = 'empresas'

# cada uno de los tipos de usuarios actores del sistema
# valores que guarda tipos_usuario 1. superadmin 2.admin de cada empresa 3.facturadores de cada empresa


class tipos_usuario(models.Model):
    tipo_usuario = models.CharField(max_length=30)

    class Meta:
        db_table = 'tipos_usuario'


# cada uno de los usuarios que acceden al sistema Superadmin, admin de las empresas y facturadores
class Usuario(models.Model):
    identificacion = models.CharField(max_length=20)
    id_empresa = models.ForeignKey('Empresa', models.SET_NULL, null=True)
    id_tipo_usuario = models.ForeignKey(
        'tipos_usuario', models.SET_NULL, null=True)

    class Meta:
        db_table = 'usuarios'

# tabla de tipos de iva que puede tener un producto
# valores que guarda tipos iva 1. 0 (exento) 2. 5%  3. 10%  4. 19%


class tipos_iva(models.Model):
    procentaje = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'tipos_iva'


# cada uno de los productos que facturan las empresas que utilizan la plataforma
# los productos se cargan por empresa
class Producto(models.Model):
    referencia = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=20)
    valor_unitario = models.FloatField(blank=True, null=True)
    id_tipo_iva = models.ForeignKey('tipos_iva', models.SET_NULL, null=True)
    nit_empresa = models.ForeignKey('Empresa', models.SET_NULL, null=True)
    cantidad = models.IntegerField(default=1, blank=True, null=True)
    id_factura = models.ForeignKey('Factura', models.SET_NULL, null=True)
    class Meta:
        db_table = 'productos'

# los documentos soporte de la venta de cada empresa cuando factura


class Factura(models.Model):
    prefijo_factura = models.CharField(max_length=20)
    numero_factura = models.IntegerField()
    numero_completo = models.CharField(max_length=100, null=True, blank=True)
    id_empresa = models.ForeignKey('Empresa', models.SET_NULL, null=True)
    fecha_factura = models.DateField()
    valor_factura = models.FloatField(blank=True, null=True)
    valor_iva = models.FloatField(blank=True, null=True)
    nit_receptor = models.CharField(max_length=20)
    estado_factura = models.ForeignKey(
        'estados_factura', models.SET_NULL, null=True)

    class Meta:
        db_table = 'facturas'


class estados_factura(models.Model):
    estado = models.CharField(max_length=50)

# cada uno de los productos que contiene una factura


class detalles_factura(models.Model):
    factura = models.ForeignKey('Factura', models.CASCADE)
    id_producto = models.ForeignKey('Producto', models.SET_NULL, null=True)
    cantidad = models.IntegerField()
    valor_unitario = models.FloatField(blank=True, null=True)
    porcentaje_iva = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'detalles_factura'
