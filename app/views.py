import django_filters.rest_framework
from rest_framework import viewsets, generics
from rest_framework import filters
from .models import Producto, Factura
from .serializers import ProductoSerializer, FacturaSerializer

# Create your views here.
class FacturaViewSet(viewsets.ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer

'''
class ProductoViewList(generics.ListCreateAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['nit_empresa']

    def post(self, request, *args, **kwargs):
        #to_user = User.objects.get(username=request.data['to_user_username'])
        #add_qs = FriendshipRequest.objects.get_or_create(from_user=request.user, to_user=to_user , message='')
        #serializer = self.get_serializer(add_qs)
        return Response(serializer.data)
'''
class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

    def get_queryset(self):
        return Producto.objects.filter(nit_empresa='11')

class FacturaViewSet(viewsets.ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer

    def get_queryset(self):
        return Factura.objects.all() 
    