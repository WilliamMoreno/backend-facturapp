from rest_framework import routers
from django.urls import path
from django.conf.urls import url, include
from .views import ProductoViewSet, FacturaViewSet

router = routers.SimpleRouter()
#router.register(r'factura', FacturaViewSet)

urlpatterns = [
    path('', include(router.urls)),
    #path('producto', ProductoViewList.as_view(), name="producto"),
    url(
        r'^producto/$',
        ProductoViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='producto',
    ),
    url(
        r'^factura/$',
        FacturaViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='factura',
    ),
]
